export interface TokenPair {
	jwt: string;
	refreshToken: string;
	expiresAt: number;
}
