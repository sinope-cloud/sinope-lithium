import { StyleSheet } from 'react-native';

export const DrawerMenuStyles = StyleSheet.create({
	root: {
		backgroundColor: '#ffffff',
		flex: 1,
	},
});
