import { StyleSheet } from 'react-native';

export const FirstProfileScreenStyles = StyleSheet.create({
    root: {
		flex: 1,
		padding: 24,
		justifyContent: 'center',
    },
    input: {
		alignSelf: 'stretch',
		marginVertical: 8,
    },
    createButton: {
		marginLeft: 'auto',
		marginVertical: 12,
	},
	createButtonContent: {
		paddingVertical: 4,
	},
});
