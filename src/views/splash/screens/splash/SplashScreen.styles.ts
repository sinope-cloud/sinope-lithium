import { StyleSheet } from 'react-native';

export const SplashScreenStyles = StyleSheet.create({
	root: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
});
